var gifts = [{"id":1, "priority": 6, "title": "none", "img": "img/image1.jpg"},
              {"id":2, "priority": 1, "title": "Hotdog", "img": "img/image2.jpg"},
              {"id":3, "priority": 3, "title": "IceCream", "img": "img/image3.png"},
              {"id":4, "priority": 4, "title": "CocaCola", "img": "img/image4.jpg"},
              {"id":5, "priority": 1, "title": "Burger", "img": "img/image5.png"},
              {"id":6, "priority": 2, "title": "Beer", "img": "img/image6.jpg"}
            ];

var congrats = "Вы выиграли приз";
var noGift = "К сожалению, вы ничего не выиграли. Попробуйте еще раз.";
var codeGen = "228-88/14-666-"

var colors = ["#47b5dc", "#b8f2fd"];
var sections = gifts.slice().reverse();
var fortune = gifts.slice();

function repaint(angle) {
    canvas.width = innerWidth;
    canvas.height = innerHeight;
    var r = Math.min(innerWidth, innerHeight) / 3;
    var cx = innerWidth/2, cy = innerHeight/2;
    var ctx = canvas.getContext("2d");
    var g = ctx.createRadialGradient(cx, cy, 0, cx, cy, r);
    var selected = (Math.floor((- 0.2 - angle) * sections.length / (2*Math.PI))
                    % sections.length);
    if (selected < 0) selected += sections.length;
    for (var i=0; i<sections.length; i++) {
        var a0 = angle + 2*Math.PI*i/sections.length;
        var a1 = a0 + 2*Math.PI/(i == 0 ? 1 : sections.length);
        var a = angle + 2*Math.PI*(i+0.5)/sections.length;
        ctx.beginPath();
        ctx.moveTo(cx, cy);
        ctx.arc(cx, cy, r, a0, a1, false);
        ctx.fillStyle = colors[i % 2];
        ctx.fill();
        ctx.fillStyle = g;
        ctx.fill();
        ctx.save();
        i == selected ? ctx.fillStyle = "#F00" : ctx.fillStyle = "#f8db32";
        ctx.font = "bold " + r/sections.length*0.6 + "px serif";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.translate(cx, cy);
        ctx.rotate(a);
        ctx.fillText(sections[i].title, r*0.62, 0);
        ctx.restore();
    }
    ctx.shadowOffsetX = 1;
    ctx.shadowOffsetY = 1;
    ctx.shadowBlur = 1;
    ctx.beginPath();
    ctx.arc(cx, cy, r*1.025, 0, 2*Math.PI, true);
    ctx.arc(cx, cy, r*0.975, 0, 2*Math.PI, false);
    ctx.fillStyle = "#f8db32";
    ctx.fill();
    ctx.shadowOffsetX = r/40;
    ctx.shadowOffsetY = r/40;
    g.addColorStop(0.2, "#f8db32");
    ctx.beginPath();
    ctx.arc(cx, cy, r/5.5, 0, 2*Math.PI, false);
    ctx.fill();
    ctx.translate(cx, cy);
    ctx.rotate(Math.PI - 0.2);
    ctx.beginPath();
    ctx.moveTo(- r*1.1, - r*0.05);
    ctx.lineTo(- r*0.9, 0);
    ctx.lineTo(- r*1.1, r*0.05);
    ctx.fillStyle = "#F44";
    ctx.fill();
}

//Adding a priority
var priority = fortunePriority(fortune);
var min = 0;
var max = fortune.length - 1;

function fortunePriority(fortune) {
  this.fortune = fortune;
  fortune.forEach(function (fortuneItem) {
    this.id = fortuneItem.id;
    this.priority = fortuneItem.priority * fortuneItem.priority;
    for (i=0; i<priority; i++) {
      fortune.push(fortuneItem);
    }
  });
}

//Choosing prize
function randomPrize(fortune) {
  this.fortune = fortune;
  this.min = min;
  this.max = max;
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var randomPrize = randomPrize(fortune);
console.log(fortune[randomPrize].title);

//More rotations if no prize
var winningID = fortune[randomPrize].id;
if (winningID === 1) winningID = 8;

var spinned = false;
var angle = 0;

//Spinning the wheel
function spin() {
  var e = 0;
  var duration = 4000;
  var speed = winningID / 10;
  function frame() {
    e += 27.8;
    var t = e / duration;
    if (t > 1) t = 1;
    angle += speed * (1 - t);
    repaint(angle);
    t < 1 ? setTimeout(frame, 10) : setTimeout(openModal(fortune[randomPrize]), 1000);
  }
  setTimeout(frame, 10);
}

//Touch/Click event
$('#canvas').on({ 'touchstart click' : function() {
  if (!spinned) {
    spin();
    spinned = true;
  }
}
});

repaint(angle);

setInterval(function() {
    if (canvas.width != innerWidth || canvas.height != innerHeight) {
        repaint(angle);
    }
}, 10);

//Showing modal window
function openModal (item) {
	$('#overlay').fadeIn(400,
	 	function() {
			$('#modal_form')
				.css('display', 'block')
				.animate({opacity: 1, top: '50%'}, 200);
      if (item.title === 'none') {
        $('#modal_form p').html(noGift);
        $('img').attr('src', item.img);
      } else {
        $('#modal_form p').html(congrats + ': ' + item.title);
        $('img').attr('src', item.img);
        $('.code-block').css('display', 'block');
        $('#code').html(codeGen + item.id);
      }
    })
}

//Close modal window
$('#modal_close, #overlay').click( function() {
  $('#modal_form')
    .animate({opacity: 0, top: '45%'}, 200,
      function() {
        $(this).css('display', 'none');
        $('#overlay').fadeOut(400);
      }
    );
});
